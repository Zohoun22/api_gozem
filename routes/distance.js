const express = require('express')
const router = express.Router()
const distanceController = require('../controllers/distance')
router.post('/get_distance_and_time',distanceController.getDistanceAndTime)
module.exports = router;