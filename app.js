const express = require('express');
const bodyParser = require('body-parser');
const app = express()
const route = express.Router()
const distanceRoutes = require('./routes/distance');
//Pour éviter les erreur de CORS
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
  });
app.use(bodyParser.json());
app.use('/api', distanceRoutes);
module.exports = app;