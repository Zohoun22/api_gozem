const { Client } = require("@googlemaps/google-maps-services-js");

const client = new Client({});

exports.getDistanceAndTime = (req, res, next) => {
  if (!req.body.start || !req.body.start || !req.body.units ||isNaN(parseFloat(req.body.start.lng)) || isNaN(parseFloat(req.body.start.lat)) || isNaN(parseFloat(req.body.start.lat)) || isNaN(parseFloat(req.body.start.lng))) {
    res.status(400).json({
      message : 'Bad request'
    })
  }
  const { start, end } = req.body,
  zone = 'GMT',
  units = req.body.units,
  origins = [start],
  destinations = [end]
  getCountry(start)
    .then((startCountry) => {

      getTimeZone(start)
        .then((startTimeZone) => {

          getCountry(end)
            .then((endCountry) => {

              getTimeZone(end)
                .then((endTimeZone) => {

                  getDistance(origins, destinations, units)
                    .then((distance) => {
                      res.status(201).json({
                        start: {
                          country: startCountry,
                          timezone: zone + '+' + startTimeZone,
                          location:start
                        },
                        end: {
                          country: endCountry,
                          timezone: zone + '+' + endTimeZone,
                          location:end
                        },
                        distance: {
                          value: (distance[0].distance.text.split(' '))[0],
                          units: (distance[0].distance.text.split(' '))[1]
                        },
                        time_diff: {
                          value: Math.abs(parseFloat(startTimeZone) - parseFloat(endTimeZone)),
                          units: 'hours'
                        }
                      });
                    })
                    .catch((e) => {
                      res.status(401).json(e)
                    });
                })
                .catch((e) => {
                  res.status(401).json(e)
                });
            })
            .catch((e) => {
              res.status(401).json(e)
            });
        })
        .catch((e) => {
          res.status(401).json(e)
        });

    })
    .catch((e) => {
      res.status(401).json(e)
    });
}


function getCountry(site) {
  return client
    .reverseGeocode({
      params: {
        latlng: `${site.lat}, ${site.lng}`,
        key: process.env.API_KEY,
      },
      timeout: process.env.TIMEOUT_LIMIT, // milliseconds
    })
    .then((r) => {
      return ((r.data.plus_code.compound_code.split(','))[(r.data.plus_code.compound_code.split(',')).length - 1]).trimStart().trim()

    })
    .catch((e) => {
      return e;
    });
}

function getTimeZone(site) {
  var date = new Date();
  var timestamp = date.getTime();
  return client
    .timezone({
      params: {
        location: `${site.lat}, ${site.lng}`,
        timestamp: timestamp / 1000,
        key: process.env.API_KEY,
      },
      timeout: process.env.TIMEOUT_LIMIT, // milliseconds
    })
    .then((r) => {
      return r.data.dstOffset === 0 ? r.data.rawOffset / 3600 : r.data.dstOffset / 3600

    })
    .catch((e) => {
      return e;
    });
}
function getDistance(origins, destinations, units) {
  return client
    .distancematrix({
      params: {
        origins: origins,
        destinations: destinations,
        key: process.env.API_KEY,
        mode: 'walking',
        units: units
      },
      timeout: process.env.TIMEOUT_LIMIT, // milliseconds
    })
    .then((r) => {
      return r.data.rows[0].elements
    })
    .catch((e) => {
      return e.response.data.error_message;
    });
}